package com.hunt.demo.models;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Card {
	private String name;
	
	private String scryfallId;
	private ImageUris image_uris;
	private List<String> multiverse_ids;
	
	@Id
	private String _id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty("scryfallId")
	public String getScryfallId() {
		return this.scryfallId;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.scryfallId = id;
	}
	
	public ImageUris getImage_uris() {
		return image_uris;
	}
	
	public void setImage_uris(ImageUris image_uris) {
		this.image_uris = image_uris;
	}
	
	public List<String> getMultiverse_ids() {
		return multiverse_ids;
	}
	
	public void setMultiverse_ids(List<String> multiverse_ids) {
		this.multiverse_ids = multiverse_ids;
	}
	
	public String get_id() {
		return _id;
	}
	
	public void set_id(String _id) {
		this._id = _id;
	}		
}
