package com.hunt.demo.models;

public class SearchResponse {
	private Integer total_cards;
	private Boolean has_more;
	private String next_page;	
	private Card[] data;
	
	public SearchResponse() { }
	
	public Integer getTotal_cards() {
		return total_cards;
	}
	public void setTotal_cards(Integer total_cards) {
		this.total_cards = total_cards;
	}
	public Boolean getHas_more() {
		return has_more;
	}
	public void setHas_more(Boolean has_more) {
		this.has_more = has_more;
	}
	public String getNext_page() {
		return next_page;
	}
	public void setNext_page(String next_page) {
		this.next_page = next_page;
	}
	public Card[] getData() {
		return data;
	}
	public void setData(Card[] data) {
		this.data = data;
	}
	
	
}
