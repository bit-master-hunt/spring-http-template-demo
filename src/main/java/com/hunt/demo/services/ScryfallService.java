package com.hunt.demo.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.hunt.demo.models.Card;
import com.hunt.demo.models.SearchResponse;

@Service
public class ScryfallService {

	public Card getCardByScryfallId( String scryfallId ) {			
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Card> response = restTemplate.getForEntity(
				"https://api.scryfall.com/cards/" + scryfallId,
				Card.class
			);
				
			return response.getBody();
		} catch( Exception e ) {
			return null;
		}
	}

	public List<Card> search( String q, Integer page ) {		
		String url = "https://api.scryfall.com/cards/search";

		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
			        .queryParam("page", page )
			        .queryParam("q", q );
	
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<SearchResponse> response = restTemplate.getForEntity( builder.toUriString(),
				SearchResponse.class
			);
			
			return Arrays.asList( response.getBody().getData() );
		} catch( Exception e ) {
			return new ArrayList<Card>();
		}
	}

}
