package com.hunt.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hunt.demo.models.Card;
import com.hunt.demo.repositories.CardRepository;

@Service
public class CardService {
	@Autowired
	private CardRepository cardRepo;
	
	@Autowired 
	private ScryfallService scryService;
	
	public Card getCardByScryfallId( String scryfallId ) {
		Optional<Card> result = cardRepo.findByScryfallId( scryfallId );
		
		if( !result.isPresent() ) {
			Card c = this.scryService.getCardByScryfallId(scryfallId);
			
			
			if( c != null ) {
				this.cardRepo.save( c );
				result = Optional.of( c );
			}			
		}
		
		return result.orElse( null );
	}
	
	
	public List<Card> search( String q, Integer page ) {
		return this.scryService.search( q, page );
	}
}
