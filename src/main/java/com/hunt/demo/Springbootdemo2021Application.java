package com.hunt.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdemo2021Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootdemo2021Application.class, args);
	}
}
