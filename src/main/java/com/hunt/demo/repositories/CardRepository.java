package com.hunt.demo.repositories;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.demo.models.Card;

public interface CardRepository extends MongoRepository<Card, String> {
	Optional<Card> findByScryfallId( String id );
}
