package com.hunt.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.demo.models.Card;
import com.hunt.demo.services.CardService;

@RestController
@RequestMapping("api/v1/cards")
public class CardController {
	@Autowired 
	private CardService cardService;

	
	@RequestMapping( value="/{scryId}", method=RequestMethod.GET )
	public Card getByScryfallId( @PathVariable String scryId ) {
		return this.cardService.getCardByScryfallId( scryId );		
	}
	
	@RequestMapping( method=RequestMethod.GET )
	public List<Card> search( 
			@RequestParam(required=false, defaultValue="") String q, 
			@RequestParam(required=false, defaultValue = "1") Integer page) {
		return this.cardService.search( q, page );		
	}
	
}
