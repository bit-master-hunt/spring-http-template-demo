package com.hunt.demo.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
public class MongoConfig {
	private static final String databaseName = "scryer";


	public @Bean MongoDatabaseFactory mongoDatabaseFactory() {
		MongoClient client = MongoClients.create("mongodb://localhost:27017");
		return new SimpleMongoClientDatabaseFactory(client, databaseName );
	}
		   
}
